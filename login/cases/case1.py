from lib.webui import *

class c1:

    #套件初始化
    def setup(self):
        open_browser()


    #套件清除
    def teardown(self):
        wd = GSTORE['wd']
        wd.quit()



    #数据驱动
    ddt_cases = [
        {
            'name': '登录-001',
            'para': ['abcd', '123456', '错误原因：该用户不存在，请注意字母大小写是否输入正确！']
        },
        {
            'name': '登录-002',
            'para': [None, '123456', '错误原因：密码错误，请注意字母大小写是否输入正确！！']
        },
        {
            'name': '登录-003',
            'para': ['renjiang', None, '错误原因：密码错误，请注意字母大小写是否输入正确！！']
        },
        {
            'name': '登录-003',
            'para': ['renjiang', '123456', 'renjiang，欢迎您到来！']
        }
    ]

    # 测试用例步骤
    def teststeps(self):


        STEP(1,'输入用户名和密码登录')

        wd = GSTORE['wd']

        username, password, info = self.para

        if username is not None:
            wd.find_element(By.ID, 'username').send_keys(username)

        if password is not None:
            wd.find_element(By.ID, 'password').send_keys(password)

        wd.find_element(By.XPATH, "//input[@value='登陆']").click()
        time.sleep(2)
        SELENIUM_LOG_SCREEN(wd,width='80%')

        STEP(2,'验证登录之后弹出页面的文本信息')

        text = wd.find_element(By.XPATH, "//div[@class='content']").text
        text = text.split()[0]
        INFO(text)

        CHECK_POINT('弹出提示与预期结果一致', text == info)
